/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  hmr: false,
  api: 'https://drpixel.fcm.unicamp.br/api',
  web: 'https://drpixel.fcm.unicamp.br/',
  uploads: 'https://drpixel.fcm.unicamp.br/uploads/',
  root: 'https://drpixel.fcm.unicamp.br'
};
