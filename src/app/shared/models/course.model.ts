import {User} from './user.model';
import {Quiz} from './quiz.model';
import {Answer} from './answer.model';

export class Course {
  constructor(
    public id: number,
    public title: string,
    public summary: string,
    public body: string,
    public thumb: string,
    public sections: Section[],
  ) {}
}

export class Section {
  constructor(
    public id: number,
    public delta: number,
    public title: string,
    public courseContents: CourseContent[],
    public course: Course
  ) {}

  getName() {
    return this.delta + '. ' + this.title
  }
}

export class CourseContent {
  constructor(
    public id: number,
    public title: string,
    public delta: number,
    public content: Content,
    public courseSection: Section,
    public quiz: Quiz
  ) {}
  getName() {
    return `${this.courseSection.delta}.${this.delta} ${this.title}`
  }
}

export class Content {
  constructor(
    public id: number,
    public title: string,
    public body: string,
    public videoPath?: string,
    public urlVideo?: string,
  ) {}
}

export class CourseUser {
  constructor(
    public id: number,
    public course: Course,
    public progresses: Progress[],
    public user: User,
    public type: number,
    public status: number,
  ) {}

  getTypeLabel(): string {
    switch (this.type) {
      case 0: return 'Padrão'; break;
      case 1: return 'Extecamp'; break;
    }
  }

  getStatusLabel(): string {
    switch (this.status) {
      case 0: return 'Pendente'; break;
      case 1: return 'Solicitado'; break;
      case 2: return 'Concluído'; break;
    }
  }
}

export class Progress {
  constructor(
    public id: number,
    public courseUser: CourseUser,
    public status: string,
    public courseContent: CourseContent,
    public answers: Answer[],
  ) {}
}

