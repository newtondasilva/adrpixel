import {CourseUser} from './course.model';

export class User {
  constructor(
    public id: number,
    public courseUsers: CourseUser[],
    public googleId: string,
    public name: string,
    public firstName: string,
    public lastName: string,
    public email: string,
    public picture: string,
    public role: Role
  ) {}
}

export class Role {
  constructor(
    public id: number,
    public name: string
  ) {}
}
