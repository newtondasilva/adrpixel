import {QuestionItem, Quiz} from './quiz.model'
import {User} from './user.model'

export class Answer {
  constructor(
    public quiz?: Quiz,
    public user?: User,
    public answerItems?: AnswerItem[],
  ) {}
}

export class AnswerItem {
  constructor(
    public questionItem: QuestionItem,
    public created: number,
    public answer: Answer,
  ) {}
}
