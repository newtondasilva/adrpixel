export class Quiz {
  constructor(
    public id: number,
    public body: string,
    public title: string,
    public questions: Question[],
  ) {}
}

export class Question {
  constructor(
    public id: number,
    public text: string,
    public questionItems: QuestionItem[],
    public correctFeedback: string,
    public wrongFeedback: string,
  ) {}

}

export class QuestionItem {
  constructor(
    public id: number,
    public question: Question,
    public delta: number,
    public text: string,
    public correct: boolean,
    public feedback: string,
  ) {}
}
