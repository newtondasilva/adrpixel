import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {User} from '../models/user.model';
import {DRPIXEL_API} from '../../app.api';

import {AuthService, GoogleLoginProvider, SocialUser} from 'angularx-social-login';
import {CourseUser, Progress} from '../models/course.model';
import {SidebarService} from './sidebar.service';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  user: User
  userChange: Subject<User> = new Subject<User>()

  constructor(private http: HttpClient,
              private authService: AuthService,
              private sidebarService: SidebarService,
              private router: Router) {
    this.userChange.subscribe( (value) => this.user = value)
  }

  /**
   * user request
   * @param socialUser
   */
  loadUser(socialUser: SocialUser): Observable<User> {
    // console.log(`loading user ${socialUser.email}`)
    let params: HttpParams
    params = new HttpParams()
      .append('group', 'show')
      .append('name', socialUser.name)
      .append('googleId', socialUser.id)
      .append('picture', socialUser.photoUrl)
      .append('firstName', socialUser.firstName)
      .append('lastName', socialUser.lastName)
      .append('email', socialUser.email)
    return this.http.get<User>(`${DRPIXEL_API}/users`, {params: params})
  }

  setUser(user: User) {
    this.userChange.next(user)
  }

  logOut() {
    this.authService.signOut(true)
    this.sidebarService.logout()
    this.setUser(null)
    this.router.navigateByUrl(`/`)
  }

  loginGoogle() {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID)
  }

  login(email: string, password: string): Observable<User> {
    let params = new HttpParams()
      .append('email', email)
      .append('password', password)
    return new Observable( observer => {
      this.http.get<User>(`${DRPIXEL_API}/users`, {params: params})
        .subscribe( user => {
          if (!user) {
            observer.next(null)
          } else {
            this.setUser(user)
            observer.next(user)
          }
        })
    })
  }

  /**
   *
   */
  isLogged(): boolean {
    if (this.user) {
      return true
    } else {
      return false
    }
  }

  getUser(): Observable<User> {
    return new Observable( (observer) => {
      if (!this.user) {
        this.authService.authState.subscribe( socialUser => {
          // check if user is logged
          if (!socialUser) {
            observer.next(null)
          } else {
            this.loadUser(socialUser).subscribe( (user) => {
              this.setUser(user)
              observer.next(this.user)
            })
          }
        })
      } else {
        observer.next(this.user)
      }
    })
  }

  getProgresses(courseId: number): Observable<Progress[]> {
    let params = new HttpParams()
      .append('userId', this.user.id.toString())
      .append('courseId', courseId.toString())
    return this.http.get<Progress[]>(`${DRPIXEL_API}/progress`, {params: params})
  }

  getCourseUser(courseId: number): CourseUser {
    return this.user.courseUsers.find( item => {
      return courseId.toString() === item.course.id.toString()
    })
  }

  /**
   * Verifica se usuario eh admin
   */
  isAdmin(): boolean {
    if (this.user && this.user.role && this.user.role.name === 'Admin') {
      return true
    } else {
      return false
    }
  }
}
