import { Injectable } from '@angular/core';
import {Course, CourseContent, CourseUser} from '../models/course.model';
import {Subject} from 'rxjs';
import {User} from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {
  course: Course
  courseUser: CourseUser
  courseContent: CourseContent
  progressValue: number


  courseChange: Subject<Course> = new Subject<Course>()
  courseUserChange: Subject<CourseUser> = new Subject<CourseUser>()
  courseContentChange: Subject<CourseContent> = new Subject<CourseContent>()
  progressValueChange: Subject<number> = new Subject<number>()

  constructor() {
    this.courseChange.subscribe( (value) => this.course = value)
    this.courseUserChange.subscribe( (value) => this.courseUser = value)
    this.courseContentChange.subscribe( (value) => this.courseContent = value)
    this.progressValueChange.subscribe((value) => this.progressValue = value)
  }

  setActiveCourse(course: Course, courseUser?: CourseUser, courseContent?: CourseContent) {
    this.courseChange.next(course)
    this.courseUserChange.next(courseUser)
    this.progressValueChange.next(Number(this.calculateProgressValue().toFixed(0)))

    if (courseContent) {
      this.courseContentChange.next(courseContent)
    }
  }

  calculateProgressValue() {
    let totalCourseContent = 0
    let completed = 0
    this.courseUser.course.sections.forEach(section => {
      totalCourseContent = totalCourseContent + section.courseContents.length
    })
    this.courseUser.progresses.forEach(progress => {
      if (progress.status === 'C') {
        completed++
      }
    })

    if (completed === 0) {
      return 0
    } else {
      return (completed / totalCourseContent) * 100
    }

  }

  setCourseContent(courseContent?: CourseContent) {
    console.log('updating courseContent')
    this.courseContentChange.next(courseContent)
  }

  clear() {
    this.courseUserChange.next(null)
    this.courseUser = null
    this.course = null
    this.progressValue = null
  }


  logout() {
    this.clear();
  }

}
