import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">Created with ♥ by <b><a href="https://www.fcm.unicamp.br/drpixel" target="_blank">Dr.Pixel</a></b></span>
  `,
})
export class FooterComponent {
}
