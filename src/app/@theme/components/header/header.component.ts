import { Component, Input, OnInit } from '@angular/core';
import {NbSidebarService, NbWindowService} from '@nebular/theme';

import {User} from '../../../shared/models/user.model';
import {Router} from '@angular/router';
import {UserService} from '../../../shared/services/user.service';
import {environment} from '../../../../environments/environment';
import {LoginComponent} from '../../../modules/auth/pages/login/login.component';
import {SidebarService} from '../../../shared/services/sidebar.service';
import {Course} from '../../../shared/models/course.model';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {
  @Input() position = 'normal';
  user: User
  isLogged = false
  course: Course
  progressMenu = [{title: 'Sem cursos', link: '/'}]
  menuEmpty = true

  constructor(private sidebarService: NbSidebarService,
              private sidebarService2: SidebarService,
              private userService: UserService,
              private courseSidebarService: SidebarService,
              private router: Router,
              private windowService: NbWindowService
              ) {
    this.userService.userChange.subscribe( user => {
      this.user = user
      if (user) {
        this.isLogged = this.userService.isLogged()
      }
    })
  }

  ngOnInit() {
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    return false;
  }

  goHome() {
    this.router.navigateByUrl(`/`)
  }

  logout() {
    this.userService.logOut();
    this.user = null
    this.isLogged = false
  }

  goToLogin() {
    this.windowService.open(LoginComponent, {title: 'Login'})
  }

  goToSite() {
    window.location.href = environment.web
  }




}
