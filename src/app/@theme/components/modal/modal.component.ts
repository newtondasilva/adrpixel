import {Component, Input, OnInit} from '@angular/core';
import {NbDialogService} from '@nebular/theme';

@Component({
  selector: 'drp-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  @Input() body: string
  constructor(private dialogService: NbDialogService) { }

  ngOnInit() {
    this.open(this.body)
  }

  open(dialog) {
    this.dialogService.open(dialog)
  }


}
