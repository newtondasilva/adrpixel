import { Component, OnInit } from '@angular/core';
import {Progress} from '../../../shared/models/course.model';
import {ActivatedRoute, Router} from '@angular/router';
import {CourseService} from '../../course/course.service';
import {User} from '../../../shared/models/user.model';
import {Question, Quiz} from '../../../shared/models/quiz.model';
import {SidebarService} from '../../../shared/services/sidebar.service';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Answer, AnswerItem} from '../../../shared/models/answer.model';


@Component({
  selector: 'quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent implements OnInit {
  progress: Progress
  quiz: Quiz
  user: User
  loaded: boolean
  answer: Answer
  selected: any
  dynamicForm: FormGroup
  submitted: boolean


  constructor(private activatedRoute: ActivatedRoute,
              private courseService: CourseService,
              private sidebarService: SidebarService,
              private router: Router,
              private formBuilder: FormBuilder,
              ) {
    this.submitted = false
  }

  ngOnInit() {
    this.dynamicForm = this.formBuilder.group({
      tickets: new FormArray([])
    })


    this.activatedRoute.params.subscribe( params => {
      this.courseService.userService.getUser().subscribe( user => {
        this.user = user
        this.courseService.getCourseProgressById(params['courseContent'], user).subscribe( progress => {
          this.progress = progress

          if (this.progress.answers.length > 0) {
            this.progress.answers.forEach( answer => this.answer = answer)
          } else {
            this.answer = new Answer(this.quiz, this.user, new Array())
          }

          this.quiz = this.progress.courseContent.quiz

          this.quiz.questions.forEach( question => {
            this.t.push(this.formBuilder.group({
              question: question,
              selected: [this.getAnswerSelected(question), Validators.required]
            }))
          })

          this.sidebarService.setActiveCourse(progress.courseUser.course, progress.courseUser, progress.courseContent)
          this.loaded = true

        })
      })
    })
  }

  /**
   * Find answer
   * @param question
   */
  getAnswerSelected(question: Question): string {
    let selected = ''
    this.answer.answerItems.forEach( answerItem => {
      if (answerItem.questionItem.question.id === question.id) {
        selected = answerItem.questionItem.id.toString()
      }
    })

    return selected
  }

  // convenience getters for easy access to form fields
  get f() { return this.dynamicForm.controls; }
  get t() { return this.f.tickets as FormArray; }


  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.dynamicForm.invalid) {
      return;
    }

    this.t.controls.filter(control => true).forEach(item => {
      item.value.question.questionItems
        .filter( questionItem => questionItem.id === item.value.selected).forEach( item2 => {
          this.answer.answerItems.push(new AnswerItem(item2, null, null))
        })
    })

    this.courseService.saveAnswer(this.progress, this.answer).subscribe( progress => {
      this.goToQuiz()
    })


  }

  onReset() {
    // reset whole form back to initial state
    this.submitted = false;
    this.dynamicForm.reset();
    // this.t.clear();
  }

  onClear() {
    // clear errors and reset ticket fields
    this.submitted = false;
    this.t.reset();
  }


  goToQuiz() {
    this.router.navigateByUrl(`/course/${this.progress.courseContent.courseSection.course.id}`
      + `/courseContent/${this.progress.courseContent.id}`)
  }

}
