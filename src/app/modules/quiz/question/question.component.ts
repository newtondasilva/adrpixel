import {Component, Input, OnInit} from '@angular/core';
import {Question, QuestionItem} from '../../../shared/models/quiz.model';
import {Progress} from '../../../shared/models/course.model';
import {AnswerItem} from '../../../shared/models/answer.model';

@Component({
  selector: 'quiz-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {
  @Input() question: Question
  @Input() progress: Progress
  answerItem: AnswerItem
  disableQuestions: boolean
  showFeedback: boolean
  selectedQuestionItem: QuestionItem
  correctQuestionItem: QuestionItem

  constructor() { }

  ngOnInit() {
    this.disableQuestions = false
    this.showFeedback = false
    this.question.questionItems
      .filter(questionItem => questionItem.correct === true)
      .forEach(questionItem => this.correctQuestionItem = questionItem)

    this.progress.answers.forEach( answer => {
      console.log(answer)
      answer.answerItems.forEach( answerItem => {{
        console.log(answerItem.questionItem.id)
        if (answerItem.questionItem.question.id === this.question.id) {
          this.answerItem = answerItem
          this.selectedQuestionItem = answerItem.questionItem
          this.showFeedback = true
          this.disableQuestions = true
        }
      }})
    })
  }

  /**
   *
   */
  selectedId() {
    if (this.selectedQuestionItem) {
      return this.selectedQuestionItem.id
    } else {
      return null;
    }
  }



}
