import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {QuizComponent} from './quiz/quiz.component';
import {NbAlertModule, NbRadioModule} from '@nebular/theme';
import { QuestionComponent } from './question/question.component';
import {ReactiveFormsModule} from '@angular/forms';
import {UtilsModule} from "../utils/utils.module";


const ROUTES: Routes = [
  {path: '', component: QuizComponent},
]


@NgModule({
  declarations: [QuizComponent, QuestionComponent],
  imports: [
    CommonModule,
    NbRadioModule,
    RouterModule.forChild(ROUTES),
    NbAlertModule,
    ReactiveFormsModule,
    UtilsModule,
  ]
})
export class QuizModule { }
