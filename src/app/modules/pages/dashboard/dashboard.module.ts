import { NgModule } from '@angular/core';


import { ThemeModule } from '../../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import {CourseModule} from '../../course/course.module';


@NgModule({
  imports: [
    ThemeModule,
    CourseModule,
  ],
  declarations: [
    DashboardComponent,
  ],
})
export class DashboardModule { }
