import {Component, OnInit} from '@angular/core';

import {CourseService} from '../../course/course.service';
import {Course} from '../../../shared/models/course.model';
import {NbSidebarService} from '@nebular/theme';
import {SidebarService} from '../../../shared/services/sidebar.service';
import {PwaService} from '../../../shared/services/pwa.service';
import {User} from '../../../shared/models/user.model';
import {UserService} from '../../../shared/services/user.service';


@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {
  public courses: Course[] = []
  public user: User

  constructor(private courseService: CourseService,
              private nbSidebarService: NbSidebarService,
              public pwaService: PwaService,
              public userService: UserService,
              private sidebarService: SidebarService) {
  }
  ngOnInit() {
    this.nbSidebarService.collapse();
    this.sidebarService.clear();

    this.userService.getUser().subscribe( user => {
      this.courseService.courses(null, user).subscribe(courses => {
        console.log(courses);
        this.courses = courses
      })

    })
  }

  installPwa(): void {
    this.pwaService.promptEvent.prompt();
  }


}
