import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {CourseContentDetailComponent} from '../content/pages/course-content-detail/course-content-detail.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    { path: 'auth', loadChildren: 'app/modules/auth/auth.module#AuthModule' },
    {path: 'dashboard', component: DashboardComponent},
    {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
    {path: 'course', loadChildren: 'app/modules/course/course.module#CourseModule'},
    {path: 'course/:course/courseContent/:courseContent/quiz', loadChildren: 'app/modules/quiz/quiz.module#QuizModule'},
    {path: 'course/:course/courseContent/:courseContent', component: CourseContentDetailComponent},
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
