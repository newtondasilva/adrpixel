import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from 'angularx-social-login';
import {UserService} from '../../../../shared/services/user.service';

@Component({
  selector: 'logout',
  templateUrl: './logout.component.html'
})
export class LogoutComponent implements OnInit {

  constructor(private authService: AuthService,
              private router: Router,
              private userService: UserService) { }

  ngOnInit() {
    this.logout()
  }

  logout() {
    this.authService.signOut(true)
    this.userService.logOut();
    this.router.navigateByUrl('/')
  }

}
