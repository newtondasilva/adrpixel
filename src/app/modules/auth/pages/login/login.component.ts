import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
import {UserService} from '../../../../shared/services/user.service';
import {NbWindowRef} from '@nebular/theme';

import {ENV} from '../../../../app.api';
import {User} from '../../../../shared/models/user.model';
import {Router} from '@angular/router';


@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  strategy: string;
  user: User
  register: string
  resetpass: string
  submitted = false
  loading = false
  invalidLogin = false
  loginForm: FormGroup

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private windowRef: NbWindowRef,
              private router: Router,
              ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    })

    this.register = `${ENV.web}user/new`
    this.resetpass = `${ENV.web}resetpass`

  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    console.log(this.f)
    this.userService.login(this.f.email.value, this.f.password.value)
      .subscribe( user => {
      if (user) {
        console.log('logged')
        this.windowRef.close()
        this.router.navigateByUrl('/')
      } else {
        console.log('log fail')
        this.invalidLogin = true
      }
    })
    this.loading = true

  }

  loginGoogle() {
    this.windowRef.close();
    this.userService.loginGoogle()
  }



}
