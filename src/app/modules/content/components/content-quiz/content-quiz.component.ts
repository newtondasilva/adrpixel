import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CourseContent, Progress} from '../../../../shared/models/course.model';
import {Router} from '@angular/router';
import {CourseService} from '../../../course/course.service';
import {Answer} from '../../../../shared/models/answer.model';

@Component({
  selector: 'content-quiz',
  templateUrl: './content-quiz.component.html'
})
export class ContentQuizComponent implements OnInit {
  @Input() progress: Progress
  title: string
  percent: number
  answer: Answer
  nextCourseContent: CourseContent


  constructor(private router: Router,
              private courseService: CourseService) {
  }

  ngOnInit() {

    let totalCorrect = 0

    this.progress.answers.forEach( answer => {
      this.answer = answer
      this.answer.answerItems.forEach( answerItem => answerItem.questionItem.correct ? totalCorrect++ : true)
    })

    this.nextCourseContent = this.courseService.getNextCourseContent(this.progress.courseContent, this.progress.courseUser.course)
    let total = this.progress.courseContent.quiz.questions.length;

    if (!totalCorrect) {
      this.percent = 0
    } else {
      this.percent = (totalCorrect / total) * 100
      this.title = this.percent.toString() + '%'
    }

    // Caso nao possua resposta vai para o teste
    if (this.progress.answers.length === 0) {
      this.goToQuiz()
    }

    if (this.percent === 100) {
      this.courseService.getCourseProgressById(this.progress.courseContent.id.toString(),
        this.progress.courseUser.user, 'C').subscribe()
    }

  }

  /**
   * Vai para a pagina do Quiz
   */
  goToQuiz() {
    this.router.navigateByUrl(`/course/${this.progress.courseContent.courseSection.course.id}`
    + `/courseContent/${this.progress.courseContent.id}/quiz`)
  }

  /**
   * Limpa respostas existentes
   */
  resetQuiz() {
    this.courseService.resetAnswer(this.progress).subscribe( progress => {
      this.progress = progress
      this.goToQuiz()
    })
  }

  continueCourse() {
    this.courseService.getCourseProgressById(this.progress.courseContent.id.toString(),
      this.progress.courseUser.user, 'C')
      .subscribe( progress => {
        this.courseService.goToCourseContent(progress, this.nextCourseContent)
      })
  }
}
