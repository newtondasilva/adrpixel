import {Component, Input, OnInit} from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {CourseService} from '../../../course/course.service';
import {Progress} from '../../../../shared/models/course.model';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'content-video',
  templateUrl: './content-video.component.html',
  styleUrls: ['./content-video.component.scss']
})
export class ContentVideoComponent implements OnInit {
  @Input() progress: Progress
  videoPath: SafeResourceUrl
  loaded = false

  constructor(private courseService: CourseService,
              private sanitizer: DomSanitizer
            ) { }

  ngOnInit() {
    this.loaded = true
    const url = environment.uploads + this.progress.courseContent.content.videoPath
    this.videoPath = this.sanitizer.bypassSecurityTrustUrl(url)
  }
}
