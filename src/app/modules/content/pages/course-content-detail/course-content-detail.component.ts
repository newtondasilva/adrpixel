import {ChangeDetectorRef, Component, OnInit, Output} from '@angular/core';
import {Course, CourseContent, Progress} from '../../../../shared/models/course.model';
import {CourseService} from '../../../course/course.service';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../../../shared/services/user.service';
import {User} from '../../../../shared/models/user.model';
import {SidebarService} from '../../../../shared/services/sidebar.service';
import {environment} from '../../../../../environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'course-content-detail',
  templateUrl: './course-content-detail.component.html',
  styleUrls: ['./course-content-detail.component.scss']
})
export class CourseContentDetailComponent implements OnInit {
  progress: Progress
  loaded = false
  user: User
  course: Course
  isAdmin = false
  editLink: string
  nextCourseContent: CourseContent
  urlVideo: SafeResourceUrl

  constructor(private courseService: CourseService,
              private activatedRoute: ActivatedRoute,
              private userService: UserService,
              private cdref: ChangeDetectorRef,
              private sanitizer: DomSanitizer,
              private sidebarService: SidebarService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe( params => {
      this.userService.getUser().subscribe( user => {
        this.user = user
        this.isAdmin = this.userService.isAdmin()

        this.courseService.getCourseProgressById(params['courseContent'], user).subscribe( progress => {
          this.progress = progress
          this.nextCourseContent = this.courseService.getNextCourseContent(this.progress.courseContent, this.progress.courseUser.course)

          this.editLink = `${environment.web}admin/course/${this.progress.courseUser.course.id}` +
            `/content/${this.progress.courseContent.id}/edit?newtab=1`
          this.loaded = true

          if (this.progress.courseContent.content 
            && this.progress.courseContent.content.urlVideo !== null) {
            this.urlVideo = this.sanitizer.bypassSecurityTrustResourceUrl(this.progress.courseContent.content.urlVideo)
          }

          this.courseService.getCourseUser(this.progress.courseContent.courseSection.course.id, this.user)
            .subscribe(courseUser =>
              this.sidebarService.setActiveCourse(courseUser.course, courseUser, progress.courseContent)
            )
        })
      })
    })
  }

  /**
   * Define content as read
   * @param state
   */
  setRead(isRead = true, goNext = true) {
    this.courseService.getCourseProgressById(this.progress.courseContent.id.toString(),
      this.progress.courseUser.user, isRead ? 'C' : 'P')
      .subscribe(progress => {

        this.progress = progress

        this.courseService.getCourseUser(this.progress.courseContent.courseSection.course.id, this.user)
          .subscribe(courseUser =>
            this.sidebarService.setActiveCourse(courseUser.course, courseUser, progress.courseContent)
          )

        if (this.nextCourseContent && goNext) {
          this.courseService.goToCourseContent(progress, this.nextCourseContent)
        }
      })
  }

  

}
