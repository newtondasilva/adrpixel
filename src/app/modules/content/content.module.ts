import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import { ContentVideoComponent } from './components/content-video/content-video.component';
import {VgCoreModule} from 'videogular2/core';
import {VgOverlayPlayModule} from 'videogular2/overlay-play';
import {VgControlsModule} from 'videogular2/controls';
import {VgBufferingModule} from 'videogular2/buffering';
import {CourseContentDetailComponent} from './pages/course-content-detail/course-content-detail.component';
import { ContentQuizComponent } from './components/content-quiz/content-quiz.component';
import {NgCircleProgressModule} from 'ng-circle-progress';
import {UtilsModule} from '../utils/utils.module';
import {NbAccordionModule, NbButtonModule, NbCardModule, NbProgressBarModule, NbTooltipModule} from '@nebular/theme';

const ROUTES: Routes = [
  {
    path: '',
    component: CourseContentDetailComponent,
  }
]

@NgModule({
  declarations: [
    CourseContentDetailComponent,
    ContentVideoComponent,
    ContentQuizComponent,
  ],
  exports: [
    CourseContentDetailComponent,
    ContentVideoComponent,
    ContentQuizComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    UtilsModule,
    NgCircleProgressModule.forRoot({
      'radius': 80,
      'space': -10,
      'outerStrokeGradient': true,
      'outerStrokeWidth': 10,
      'outerStrokeColor': '#4882c2',
      'outerStrokeGradientStopColor': '#53a9ff',
      'innerStrokeColor': '#e7e8ea',
      'innerStrokeWidth': 10,
      'title': 'UI',
      'animateTitle': false,
      'animationDuration': 1000,
      'showUnits': false,
      'showSubtitle': true,
      'showBackground': false,
      'clockwise': true,
      'startFromZero': true
    }),
    NbProgressBarModule,
    NbAccordionModule,
    NbCardModule,
    NbTooltipModule,
    NbButtonModule,
  ],
})
export class ContentModule { }
