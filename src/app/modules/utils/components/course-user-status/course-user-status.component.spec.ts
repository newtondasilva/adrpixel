import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseUserStatusComponent } from './course-user-status.component';

describe('CourseUserStatusComponent', () => {
  let component: CourseUserStatusComponent;
  let fixture: ComponentFixture<CourseUserStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseUserStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseUserStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
