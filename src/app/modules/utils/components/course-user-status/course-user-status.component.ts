import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {CourseUser} from '../../../../shared/models/course.model';

@Component({
  selector: 'course-user-status',
  templateUrl: './course-user-status.component.html',
  styleUrls: ['./course-user-status.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CourseUserStatusComponent implements OnInit {
  @Input() courseUser: CourseUser
  constructor() { }

  ngOnInit() {
    console.log(this.courseUser)
  }

}
