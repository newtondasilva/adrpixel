import {Component, Input, OnInit} from '@angular/core';
import {CourseContent} from '../../../../shared/models/course.model';

@Component({
  selector: 'drp-content-title',
  templateUrl: './content-title.component.html'
})
export class ContentTitleComponent implements OnInit {
  @Input() courseContent: CourseContent
  constructor() { }

  ngOnInit() {
  }

}
