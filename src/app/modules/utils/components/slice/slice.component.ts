import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'drp-slice',
  templateUrl: './slice.component.html'
})
export class SliceComponent implements OnInit {
  @Input() text: string
  @Input() quiz: boolean
  @Input() size: number
  sliced: string
  constructor() { }

  ngOnInit() {
    if (this.size > 0 && this.text.length >= 30) {
      this.sliced = this.text.substr(0, this.size) + '...'
    } else {
      this.sliced = this.text
    }

  }

}
