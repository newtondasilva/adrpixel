import {Component, Input, OnInit} from '@angular/core';
import {Course, CourseContent, CourseUser} from '../../../../shared/models/course.model';
import {SidebarService} from '../../../../shared/services/sidebar.service';
import {CourseService} from '../../../course/course.service';
import {UserService} from '../../../../shared/services/user.service';
import {AuthService, GoogleLoginProvider} from 'angularx-social-login';
import {User} from '../../../../shared/models/user.model';
import {Router} from '@angular/router';



@Component({
  selector: 'course-menu',
  templateUrl: './course-menu.component.html',
  styleUrls: ['./course-menu.component.scss']
})
export class CourseMenuComponent implements OnInit {
  @Input() element
  course: Course
  courseUser: CourseUser
  activeCourseContent: CourseContent
  user: User
  isLogged: boolean

  items: any[]
  loaded = false

  constructor(private sidebarService: SidebarService,
              private courseService: CourseService,
              private userService: UserService,
              private router: Router,
              private authService: AuthService) {
    // Mount Course
    this.sidebarService.courseChange.subscribe( course => {
      this.sidebarService.courseUserChange.subscribe( courseUser => {
        this.course = course
        this.courseUser = courseUser
        this.mountCourse()
        this.mountProgress()
      })
    })
    // Mount current content
    this.sidebarService.courseContentChange.subscribe( courseContent => this.activeCourseContent = courseContent)

  }

  ngOnInit() {
    this.sidebarService.setActiveCourse(this.sidebarService.course, this.sidebarService.courseUser, this.sidebarService.courseContent)
    this.userService.getUser().subscribe( user => {
      if (user) {
        this.user = user
        this.isLogged = true
      }
    })
  }

  mountProgress() {
    this.items.forEach( sectionRow => {
      let totalCompletedSection = 0
      sectionRow.children.forEach( children => {
        this.courseUser.progresses.filter( progress => progress.courseContent.id === children.id)
          .forEach( item => {
            if (item.status === 'C') {
              totalCompletedSection++;
            }
          })

      })
      sectionRow.totalCompleted = totalCompletedSection
    })
  }

  mountCourse() {
    let children = []
    let previousCompleted = true
    let completed = false
    let disabled: boolean
    if (this.course) {
      this.course.sections.forEach( section => {
        let contents = []
        let total = 0
        // fill courseContents
        section.courseContents.forEach( courseContent => {
          total++;
          // verifica se aula ja foi competaa
          if (this.isCompleted(courseContent)) {
            completed = true
            disabled = false
          } else {
            previousCompleted ? disabled = false : disabled = true
            completed = false
          }

          contents.push({
            title: courseContent.title,
            link: `/course/${this.course.id}/courseContent/${courseContent.id}`,
            courseContent: courseContent,
            quiz: typeof courseContent.quiz !== 'undefined' ? true : false,
            video: courseContent.content ? true : false,
            isCompleted: completed,
            isDisabled: disabled,
            delta: `${section.delta}.${courseContent.delta}`,
            id: courseContent.id
          })
          previousCompleted = completed
        })
        children.push({
          title: section.title,
          total: total,
          totalCompleted: 0,
          section: section,
          link: `/course/${this.course.id}`,
          children: contents
        })
      })

      this.loaded = true
      this.items = children
    }
  }

  /**
   *
   * @param courseContent
   */
  isCompleted(courseContent: CourseContent): boolean {
    this.courseUser.progresses.filter( progress => progress.courseContent.id === courseContent.id)
      .forEach( item => status = item.status)

    if (status === 'C') {
      return true
    }
    return false
  }

  /**
   *
   * @param event
   */
  toogleContent(courseContent: CourseContent, event) {
    if (event.target.checked) {
      status = 'C'
    } else {
      status = 'I'
    }

    this.courseService.getCourseProgressById(courseContent.id.toString(), this.userService.user, status)
      .subscribe(progress => {
        this.sidebarService.setActiveCourse(progress.courseUser.course, progress.courseUser)
    })
  }

  itemClass(data) {
    if (this.activeCourseContent && (data.courseContent.id === this.activeCourseContent.id)) {
      return 'active';
    }

    if (data.isDisabled) {
      return 'disabled';
    }

    return '';
  }

  logout() {
    this.userService.logOut();
    this.user = null
    this.isLogged = false
  }

  navigateTo(courseContent: CourseContent, isDisabled: boolean) {
    if (!isDisabled) {
      this.router.navigateByUrl(`/course/${this.course.id}/courseContent/${courseContent.id}`)
      this.element.close()
    }

  }

  goToLogin() {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }
}
