import {Component, OnInit, TemplateRef} from '@angular/core';
import {CourseService} from '../../../course/course.service';
import {Course, CourseContent} from '../../../../shared/models/course.model';
import {SidebarService} from '../../../../shared/services/sidebar.service';
import {ENV} from '../../../../app.api';
import {NbDialogService} from '@nebular/theme';
import {User} from '../../../../shared/models/user.model';
import {UserService} from '../../../../shared/services/user.service';


@Component({
  selector: 'drp-course-bar',
  templateUrl: './course-bar.component.html',
  styleUrls: ['./course-bar.component.scss']
})
export class CourseBarComponent implements OnInit {
  private course: Course
  public user: User
  public courseContent: CourseContent
  private thumbnail: string
  public loaded: boolean
  private value = 0

  constructor(private courseService: CourseService,
              private sidebarService: SidebarService,
              private userService: UserService,
              private dialogService: NbDialogService,
              ) {
    this.loaded = false
    this.sidebarService.courseContentChange.subscribe( courseContent => {
      this.courseContent = courseContent
    })
    this.sidebarService.progressValueChange.subscribe(value => this.value = value)
    this.userService.userChange.subscribe( value => this.user = value)

    this.sidebarService.courseChange.subscribe( course => {
      this.course = course
      this.thumbnail = ENV.uploads + this.course.thumb
      this.loaded = true
    })
  }

  open(dialog: TemplateRef<any>, closeOnBackdropClick, hasScroll) {
    this.dialogService.open(dialog, { closeOnBackdropClick, hasScroll})
  }

  ngOnInit() {

  }

  getStatus() {
    if (this.value <= 25) {
      return 'danger';
    } else if (this.value <= 50) {
      return 'warning';
    } else if (this.value <= 75) {
      return 'info';
    } else {
      return 'success';
    }
  }
}
