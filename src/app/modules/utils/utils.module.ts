import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SliceComponent} from './components/slice/slice.component';
import { ContentTitleComponent } from './components/content-title/content-title.component';
import {CourseMenuComponent} from './components/course-menu/course-menu.component';
import {NbAccordionModule, NbActionsModule, NbCardModule, NbCheckboxModule, NbProgressBarModule} from '@nebular/theme';
import {RouterModule} from '@angular/router';
import {CourseBarComponent} from './components/course-bar/course-bar.component';
import {NgCircleProgressModule} from 'ng-circle-progress';
import { CourseUserStatusComponent } from './components/course-user-status/course-user-status.component';

@NgModule({
  declarations: [
    SliceComponent,
    ContentTitleComponent,
    CourseMenuComponent,
    CourseBarComponent,
    CourseUserStatusComponent
  ],
    exports: [
        SliceComponent,
        ContentTitleComponent,
        CourseMenuComponent,
        CourseBarComponent,
        CourseUserStatusComponent
    ],
  imports: [
    CommonModule,
    NbActionsModule,
    NbAccordionModule,
    NbCheckboxModule,
    RouterModule,
    NbProgressBarModule,
    NbCardModule,
    NgCircleProgressModule,
  ],
})
export class UtilsModule { }
