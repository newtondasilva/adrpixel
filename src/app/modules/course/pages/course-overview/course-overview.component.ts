import {Component, OnInit} from '@angular/core';
import {CourseContent, Course, CourseUser} from '../../../../shared/models/course.model';
import {CourseService} from '../../course.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ENV} from '../../../../app.api';

import {UserService} from '../../../../shared/services/user.service';
import {User} from '../../../../shared/models/user.model';
import {SidebarService} from '../../../../shared/services/sidebar.service';
import {LoginComponent} from '../../../auth/pages/login/login.component';
import {NbWindowService} from '@nebular/theme';



@Component({
  selector: 'course-overview',
  templateUrl: './course-overview.component.html',
  styleUrls: ['./course-overview.component.scss']
})
export class CourseOverviewComponent implements OnInit {
  course: Course
  thumbnail: string
  courseUser: CourseUser
  nextContent: CourseContent
  user: User
  loaded = false

  constructor(private courseService: CourseService,
              private route: ActivatedRoute,
              private router: Router,
              private userService: UserService,
              private windowService: NbWindowService,
              private sidebarService: SidebarService) {

  }

  ngOnInit() {
    this.sidebarService.setCourseContent(null)
    this.route.firstChild.params.subscribe((params: any) => {
      if (params.hasOwnProperty('course')) {
        this.userService.getUser().subscribe( user => {
          this.courseService.getCourseById(params.course).subscribe(course => {
            // verifica se ta logado
            if (user) {
              this.user = user
              this.courseService.getCourseUser(course.id, user).subscribe( courseUser => {
                this.courseUser = courseUser
                this.nextContent = this.courseService.getNextCourseProgress(courseUser)
                this.sidebarService.setActiveCourse(this.courseUser.course, this.courseUser)
              })
            }
            this.mountCourse(course)
          })

        })
      }
    })


  }

  /**
   * @param course
   */
  mountCourse(course: Course) {
    this.course = course
    this.thumbnail = ENV.uploads + this.course.thumb
    this.loaded = true
  }

  hasCourseUser() {
    if (this.courseUser) {
      return true
    } else {
      return false
    }
  }

  contentStatus(content: CourseContent): string {
    status = 'I'
    if (!this.courseUser.progresses) {
      return null
    }

    this.courseUser.progresses.filter( progress => progress.courseContent.id === content.id)
      .forEach( item => status = item.status)

    return status
  }

  goNextContent() {
    this.router.navigateByUrl(`/course/${this.course.id}/courseContent/${this.nextContent.id}`)
  }

  participate() {
    this.courseService.getCourseUser(this.course.id, this.user).subscribe(courseUser => this.courseUser = courseUser)
  }

  login() {
    this.windowService.open(LoginComponent, {title: 'Login'})
  }

}
