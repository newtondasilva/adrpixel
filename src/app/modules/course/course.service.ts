import {Observable} from 'rxjs';
import { Injectable } from '@angular/core';
import {CourseContent, Course, CourseUser, Progress} from '../../shared/models/course.model';
import {HttpClient, HttpParams} from '@angular/common/http';
import {DRPIXEL_API} from '../../app.api';
import {User} from '../../shared/models/user.model';
import {Answer} from '../../shared/models/answer.model';
import {Router} from '@angular/router';
import {UserService} from '../../shared/services/user.service';


@Injectable()
export class CourseService  {

  constructor(private http: HttpClient,
              private router: Router,
              public userService: UserService) {
    this.userService.getUser()
  }

  /**
   * Obtem lista de cursos
   * @param search
   */
  courses(search?: string, user?: User): Observable<Course[]> {
    let params = new HttpParams().append('userId', user ? user.id.toString() : null)

    // q padrao do json-server que busca em todos os parametros
    return this.http.get<Course[]>(`${DRPIXEL_API}/courses`, {params: params})
  }

  /**
   * Obtem curso por id
   * @param id
   */
  getCourseById(id: string): Observable<Course> {
    let params = new HttpParams().append('group', 'show');
    return this.http.get<Course>(`${DRPIXEL_API}/courses/${id}`, {params: params})
  }

  /**
   * Inclui courseUser
   * @param course
   * @param user
   */
  getCourseUser(courseId: number, user: User): Observable<CourseUser> {
    let params = new HttpParams()
      .append('courseId', courseId.toString())
      .append('userId', user.id.toString())
    return this.http.get<CourseUser>(`${DRPIXEL_API}/courseUser`, {params: params})
  }

  /**
   * Obtem proxima aula
   * @param courseUser
   * @param course
   */
  getNextCourseProgress(courseUser: CourseUser, currentContent?: CourseContent): CourseContent {
    let nextContent = null

    courseUser.course.sections.forEach( section => {
      section.courseContents.forEach(content => {
        if (nextContent == null) {
          nextContent = content

          // verificacao senao eh o conteudo atual
          if (currentContent && content.id === currentContent.id) {
            nextContent = null
          }

          // filtrar progress pelo proximo
          courseUser.progresses.filter( progress => progress.courseContent.id === content.id)
            .forEach( progress => {
              if (progress.status === 'C') {
                nextContent = null
              }
            })
        }
      })
    })

    return nextContent
  }

  /**
   * Obtem next content
   * @param currentCourseContent
   * @param courseUser
   */
  getNextCourseContent(currentCourseContent: CourseContent, course: Course): CourseContent {
    let nextCourseContent = null
    let contentPassed = false
    course.sections.forEach( section => {
      section.courseContents.forEach(content => {
        if (contentPassed && nextCourseContent === null) {
          nextCourseContent = content
          content.courseSection = section
        }
        if (content.id === currentCourseContent.id) {
          contentPassed = true
        }
      })
    })
    return nextCourseContent
  }

  /**
   * Vai para proxima aula pendente
   * @param courseUser
   * @param currentContent
   */
  goToNextCourseProgress(progress: Progress) {
    let nextContent = this.getNextCourseProgress(progress.courseUser, progress.courseContent);
    if (nextContent) {
      this.router.navigateByUrl(`/course/${progress.courseContent.courseSection.course.id}/courseContent/${nextContent.id}`)
    } else {
      this.router.navigateByUrl(`/course/${progress.courseContent.courseSection.course.id}`)
    }
  }

  goToCourseContent(progress: Progress, courseContent: CourseContent) {
    this.router.navigateByUrl(`/course/${progress.courseContent.courseSection.course.id}/courseContent/${courseContent.id}`)
  }



  getContentById(id: string, user: User, setRead = false): Observable<CourseContent> {
    let params = new HttpParams()
      .append('group', 'courseContent')
      .append('userId', user.id.toString())
      .append('setRead', setRead ? '1' : '0')
    return this.http.get<CourseContent>(`${DRPIXEL_API}/courseContent/${id}`, {params: params})
  }

  /**
   *
   * @param courseContentId
   * @param user
   * @param status
   */
  getCourseProgressById(courseContentId: string, user: User, status = null): Observable<Progress> {
    let params = new HttpParams()
    if (status) {
      params = new HttpParams().append('group', 'courseContent')
        .append('status', status)
    } else {
      params = new HttpParams().append('group', 'courseContent')
    }

    return this.http.get<Progress>(`${DRPIXEL_API}/courseContent/${courseContentId}/user/${user.id}/courseProgress`,
      {params: params})
  }

  /**
   *
   * Salva respotas do teste
   * @param questionItem
   * @param progress
   */
  saveAnswer(progress: Progress, answer: Answer): Observable<Progress> {
    let send = []
    answer.answerItems.forEach( answerItems => send.push(answerItems.questionItem.id))
    let params = new HttpParams().append('answerItems', JSON.stringify(send))
    return this.http.get<Progress>(`${DRPIXEL_API}/courseProgress/${progress.id}/answer`, {params: params})
  }

  /**
   * Limpa answers
   * @param progress
   */
  resetAnswer(progress: Progress): Observable<Progress> {
    let params = new HttpParams().append('group', 'list')
    return this.http.get<Progress>(`${DRPIXEL_API}/courseProgress/${progress.id}/resetAnswer`, { params: params})
  }



}
