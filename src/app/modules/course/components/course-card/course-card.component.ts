import {Component, Input, OnInit} from '@angular/core';
import {Course} from '../../../../shared/models/course.model';
import {ENV} from '../../../../app.api';
import {User} from '../../../../shared/models/user.model';
import {CourseService} from '../../course.service';


@Component({
  selector: 'drp-course-card',
  templateUrl: './course-card.component.html'
})
export class CourseCardComponent implements OnInit {
  @Input() course: Course

  user: User
  showContinue: boolean
  showSubscribe: boolean
  thumbnail: string
  isLogged: boolean

  constructor(private courseService: CourseService) {
  }

  ngOnInit() {
    this.courseService.userService.getUser().subscribe( user => {
      if (user) {
        this.courseService.getCourseUser(this.course.id, user)
          .subscribe(courseUser => {
            console.log(courseUser);
            if (this.courseService.getNextCourseProgress(courseUser)) {
              this.showContinue = true
            }
          })
      } else {
        this.showSubscribe = true
      }
      this.isLogged = this.courseService.userService.isLogged()
    })

    if (this.course.thumb) {
      this.thumbnail = ENV.uploads + this.course.thumb
    }
  }

}
