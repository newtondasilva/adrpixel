import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourseCardComponent } from './components/course-card/course-card.component';
import {
  NbAccordionModule,
  NbActionsModule,
  NbButtonModule,
  NbCardModule, NbCheckboxModule,
  NbListModule, NbMenuModule,
  NbProgressBarModule,
  NbTabsetModule, NbUserModule
} from '@nebular/theme';
import {CourseOverviewComponent} from './pages/course-overview/course-overview.component';
import {RouterModule, Routes} from '@angular/router';
import {CourseComponent} from './course.component';
import {CourseMenuComponent} from '../utils/components/course-menu/course-menu.component';
import {ContentModule} from '../content/content.module';
import {UtilsModule} from '../utils/utils.module';

const ROUTES: Routes = [
  {
    path: '',
    component: CourseOverviewComponent,
    children: [
      {path: ':course', component: CourseOverviewComponent}

    ]
  }
]

@NgModule({
  declarations: [
    CourseComponent,
    CourseCardComponent,
    CourseOverviewComponent,
  ],
    exports: [
        CourseComponent,
        CourseCardComponent,
        CourseOverviewComponent,
    ],
  imports: [
    UtilsModule,
    CommonModule,
    ContentModule,
    NbCardModule,
    NbCheckboxModule,
    NbAccordionModule,
    NbButtonModule,
    NbTabsetModule,
    NbListModule,
    NbActionsModule,
    NbProgressBarModule,
    RouterModule.forChild(ROUTES),
    NbMenuModule,
    NbCheckboxModule,
    NbUserModule
  ]
})
export class CourseModule { }
